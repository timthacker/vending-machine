.include "m2560def.inc"

.def temp = r16
.def temp1 = r17 // Temporary registers for all tasks
.def temp2 = r18
.def row = r19 ; current row number
.def col = r20 ; current column number
.def rmask = r21 ; mask for current row during scan
.def cmask = r22 ; mask for current column during scan
.def pressed = r23		; the keypad debounce flag
.def debounceFlag0 = r24	; button 1 debounce
.def debounceFlag1 = r25	; button 2 debounce

.equ LCD_RS = 7
.equ LCD_E = 6
.equ LCD_RW = 5
.equ LCD_BE = 4
.equ F_CPU = 16000000
.equ DELAY_1MS = F_CPU / 4 / 1000 - 4; 4 cycles per iteration - setup/call-return overhead
.equ PORTLDIR = 0xF0 ; PD7-4: output, PD3-0, input
.equ INITCOLMASK = 0xEF ; scan from the leftmost column,
.equ INITROWMASK = 0x01 ; scan from the top row
.equ ROWMASK = 0x0F ; for obtaining input from Port D

.include "macro.asm"

.dseg

DebounceCounter:		; Debounce counter. Used to determine
    .byte 2             ; if 100ms have passed
ScreenState:			; ScreenState 9: idle | 0: start screen | 1: select screen | 2: empty screen | 3: coin screen | 4: deliver screen | 5: admin screen
    .byte 1 
HalfSecondCounter:      ; count half second
	.byte 2
ThreeSecondCounter:		; counts 3s
	.byte 1
OneAndHalfSecondCounter:		; counts 1.5s
	.byte 2
FiveSecondCounter:      ; counts 5s
	.byte 2
SymbolFlag:				;0: number | 1: symbol | 2: star | 3: Hash | 4: A | 5: B | 6: C
	.byte 1
AdminFlag:              ;Checks if program is entering admin mode to default to item 1
	.byte 1

// Used for keypad output
Input:				; Stores number pressed on keypad
	.byte 1
StockNumber:        ; Stores number of stock for the current item
	.byte 1
ItemCost:           ; stores cost for the current item
	.byte 1
AdminLoopCount:     ; loop counter for waiting 5 seconds entering admin mode
	.byte 1

Inventory:          ; Array storing the inventory counts
	.byte 9
Costs:              ; Array storing the items costs
	.byte 9

.cseg

.org 0x0000
    jmp RESET
.org INT0addr
    jmp EXT_INT0        ; Push botton interrupts
.org INT1addr
	jmp EXT_INT1
    jmp DEFAULT        	; No handling for IRQ0.
    jmp DEFAULT        	; No handling for IRQ1.
.org OVF0addr
    jmp Timer0OVF      	; Jump to the interrupt handler for Timer0 overflow.
	jmp DEFAULT         ; default service for all other interrupts.

DEFAULT:  reti

RESET:
	ldi temp1, 0
	sts AdminLoopCount, temp1

    // Load default inventory values
	ldi temp, 1
	sts Inventory, temp
	inc temp
	sts Inventory + 1, temp
	inc temp
	sts Inventory + 2, temp
	inc temp
	sts Inventory + 3, temp
	inc temp
	sts Inventory + 4, temp
	inc temp
	sts Inventory + 5, temp
	inc temp
	sts Inventory + 6, temp
	inc temp
	sts Inventory + 7, temp
	inc temp
	sts Inventory + 8, temp

	// Set default costs
	ldi temp, 1
	ldi temp1, 2
	sts Costs, temp
	sts Costs + 1, temp1
	sts Costs + 2, temp
	sts Costs + 3, temp1
	sts Costs + 4, temp
	sts Costs + 5, temp1
	sts Costs + 6, temp
	sts Costs + 7, temp1
	sts Costs + 8, temp
    
	// Initialize the stack
	ldi temp, low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp

	// Initialise LCD screen
	ser temp
	out DDRF, temp
	out DDRA, temp
	clr temp
	out PORTF, temp
	out PORTA, temp
	
	// Keypad setup
    ldi temp1, PORTLDIR     ; PB7:4/PB3:0, out/in
    sts DDRL, temp1         ; PORTB is input
    
	// LED setup
	ldi temp1, 0b00000000               ; PORTC is output
    out DDRC, temp1
    out PORTC, temp1
	
	out DDRG, temp1
	out PORTG, temp1

	// LCD startup
	do_lcd_command 0b00111000 ; 2x5x7
	rcall sleep_5ms
	do_lcd_command 0b00111000 ; 2x5x7
	rcall sleep_1ms
	do_lcd_command 0b00111000 ; 2x5x7
	do_lcd_command 0b00111000 ; 2x5x7
	do_lcd_command 0b00001000 ; display off?
	do_lcd_command 0b00000001 ; clear display
	do_lcd_command 0b00000110 ; increment, no display shift
	do_lcd_command 0b00001110 ; Cursor on, bar, no blink

	// Load intial display
	do_lcd_data '2'
	do_lcd_data '1'
	do_lcd_data '2'
	do_lcd_data '1'
	do_lcd_data ' '
	do_lcd_data '1'
	do_lcd_data '7'
	do_lcd_data 's'
	do_lcd_data '1'
	do_lcd_data ' '
	do_lcd_data ' '
	do_lcd_data ' '
	do_lcd_data 'K'
	do_lcd_data '8'

	do_lcd_command 0b11000000; next line

	do_lcd_data 'V'
	do_lcd_data 'e'
	do_lcd_data 'n'
	do_lcd_data 'd'
	do_lcd_data 'i'
	do_lcd_data 'n'
	do_lcd_data 'g'
	do_lcd_data ' '
	do_lcd_data 'M'
	do_lcd_data 'a'
	do_lcd_data 'c'
	do_lcd_data 'h'
	do_lcd_data 'i'
	do_lcd_data 'n'
	do_lcd_data 'e'

	rjmp main

.include "timer0.asm"

main:
	//Initialising counters and flags for debouncing
	clear DebounceCounter
	clear ScreenState
	clear ThreeSecondCounter
	clr debounceFlag0
	clr debounceFlag1
	
	; Timer 0 init
    ldi temp, 0b00000000
    out TCCR0A, temp
    ldi temp, 0b00000010
    out TCCR0B, temp        ; Prescaling value=8
    ldi temp, 1<<TOIE0      ; = 128 microseconds
    sts TIMSK0, temp        ; T/C0 interrupt enable

	; INT0 (PB1) init
	ldi temp, (2 << ISC00)      ; set INT0 as falling-
    sts EICRA, temp             ; edge triggered interrupt
    in temp, EIMSK              ; enable INT0
    ori temp, (1<<INT0)
    out EIMSK, temp

	; INT1 (PB2) init
	ldi temp, (2 << ISC00)      ; set INT1 as falling-
    sts EICRA, temp             ; edge triggered interrupt
    in temp, EIMSK              ; enable INT1
    ori temp, (1<<INT1)
    out EIMSK, temp

	ldi cmask, INITCOLMASK ; initial column mask
	clr col ; initial column

	sei ; Enable global interrupt

// Main loop checking the program state and jumps to the various screen states
loop:
	lds temp, ScreenState
	cpi temp, 1
	breq SelectScreen
	cpi temp, 2
	breq emptyScreenMain
	cpi temp, 3
	breq InsertScreenMain
	cpi temp, 4
	breq DeliverScreenMain
	cpi temp, 5
	breq AdminScreenMain
	rjmp keypad; Not sure what this does, apparently did nothing when i added it back in

// Select item screen initialiser
SelectScreen:
	call displaySelectItem; update display for select
	jmp keypad

// Empty screen initialiser
EmptyScreenMain:
	call EmptyMode; update display for empty mode
	rjmp keypad

// Insert screen initialiser
InsertScreenMain:
	call InsertMode; update display for insert mode
	jmp keypad
	// Potentiometer code TODO
	rjmp halt

// Deliver screen initialiser
DeliverScreenMain:
	call DeliverMode; update display for deliver screen
	rjmp halt

// Admin screen initialiser
AdminScreenMain:
	call AdminMode; update display for Admin mode
	jmp keypad
	rjmp halt

halt:
	rjmp halt

// Modules
.include "display.asm"
.include "lcd.asm"
.include "buttons.asm"
.include "keypad.asm"
