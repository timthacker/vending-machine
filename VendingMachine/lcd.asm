;LCD HANDALING

lcd_command: ;function
	out PORTF, temp1 ;write out data from temp1
	rcall sleep_1ms ;one millisecond delay 
	lcd_set LCD_E ;enable read/write to LCD
	rcall sleep_1ms ;sleep again- ensure data was read in
	lcd_clr LCD_E ;disable read write
	rcall sleep_1ms ;sleep again
	ret

lcd_data:
	out PORTF, temp1
	lcd_set LCD_RS; write to data register
	rcall sleep_1ms
	lcd_set LCD_E ;enable write
	rcall sleep_1ms;wait for good measure
	lcd_clr LCD_E;disable write
	rcall sleep_1ms
	lcd_clr LCD_RS; reset back to writing to instruction reguster 
	ret

lcd_wait:
	push temp1 ;save register
	clr temp1
	out DDRF, temp1 ;set for input 
	out PORTF, temp1 ;ensure no pull up resistors
	lcd_set LCD_RW ;enable reading from LCD
lcd_wait_loop:
	rcall sleep_1ms;wait for LCD
	lcd_set LCD_E ;enable read
	rcall sleep_1ms
	in temp1, PINF ;read in LCD state
	lcd_clr LCD_E ;disable read
	sbrc temp1, 7 ;check if busy flag is cleard  
	rjmp lcd_wait_loop ;if not loop until not busy
	lcd_clr LCD_RW ; disable read, enable write
	ser temp1
	out DDRF, temp1 ;set port F for output again
	pop temp1
	ret

/*write_number: ;write number
	push temp
	push temp1

	ldi temp, '0';convert to ascii
	add temp1, temp ;number to write held in temp2
	do_lcd_data temp1
	
	pop temp1
	pop temp
	ret*/

sleep_1ms:
	push r24
	push r25
	ldi r25, high(DELAY_1MS)
	ldi r24, low(DELAY_1MS)
delayloop_1ms:
	sbiw r25:r24, 1
	brne delayloop_1ms
	pop r25
	pop r24
	ret

sleep_5ms:
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	ret