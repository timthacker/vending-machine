;macros

.macro do_lcd_command
	ldi temp1, @0 ;load register with user inputted data e.g. 0b00111000
	call lcd_command;write command into LCD instruction register
	call lcd_wait; wait for LCD to handle command
.endmacro

.macro do_lcd_data
	ldi temp1, @0 ;load register with user inputted data e.g. 'C'
	call lcd_data; write data to data reguster
	call lcd_wait; wait for LCD to handle data
.endmacro

// LCD load integer from register
.macro do_lcd_rdata
	lds temp1, @0
	ldi temp, '0'; convert to ascii
	add temp1, temp; number to write held in temp2
	rcall lcd_data
	rcall lcd_wait
.endmacro

.macro lcd_set
	sbi PORTA, @0 ;set Port A  required bit
.endmacro

.macro lcd_clr
	cbi PORTA, @0; clear Port A required bit
.endmacro

.macro clear
    ldi YL, low(@0)     ; load the memory address to Y
    ldi YH, high(@0)
    clr temp 
    st Y+, temp         ; clear the two bytes at @0 in SRAM
    st Y, temp
.endmacro