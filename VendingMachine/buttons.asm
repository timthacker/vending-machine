	; PB debounce for PB0 and PB1
	; Using 50ms timer
	;
	checkPBDebounce:				; if either flag is set - run the debounce timer
		cpi debounceFlag0, 1
		breq newPBDebounce
		cpi debounceFlag1, 1
		breq newPBDebounce
		; otherwise - don't need the debounce timer
		rjmp endPBDebounce ; go to the end of debounce

	newPBDebounce:				;	if flag is set continue counting until 50 milliseconds
		clr r26
		clr r27
		clr temp

		lds r26, DebounceCounter
    	lds r27, DebounceCounter+1
    	adiw r27:r26, 1 ; Increase the temporary counter by one.

    	cpi r26, low(50)     	; Check if (r25:r24) = 390
    	ldi temp, high(50)    	; 390
    	cpc temp, r27
    	brne notPBFifty			; 50 milliseconds have not passed

		clr debounceFlag0 		;	once 50 milliseconds have passed, set the debounceFlag to 0
		clr debounceFlag1 		;	once 50 milliseconds have passed, set the debounceFlag to 0
	   	clear DebounceCounter	; Reset the debounce counter.
		clr r26
		clr r27	; Reset the debounce counter.

	notPBFifty: 	; Store the new value of the debounce counter.
		sts DebounceCounter, r26
		sts DebounceCounter+1, r27
		rjmp endPBDebounce

EXT_INT0:
    push temp
	in temp, SREG	; Prologue starts.
    push temp       ; Save all conflict registers in the prologue.
	push temp1       			; Prologue ends.
                    ; Load the value of the temporary counter.

	; debounce check
	cpi debounceFlag0, 1		; if the button is still debouncing, ignore the interrupt
	breq END_INT	
	ldi debounceFlag0, 1		; set the debounce flag
	ser r18;
	
	lds temp, ScreenState
	cpi temp, 5
	breq IncreaseStock
	cpi temp, 2
	breq ReturnToStart
	
	
	;out DDRC, r18; set Port C for output
	;ldi r18, 0x00;
	;out PORTC, r18; write the pattern
	;ldi temp, 1
	;sts ScreenState, temp	
	rjmp END_INT

IncreaseStock:
	lds temp1, Input
	lds temp, StockNumber
	
	out DDRC, temp1
	out PORTC, temp1

	ldi XL, low(Inventory)
	ldi XH, high(Inventory)

	ldi temp2, 1
	sub temp1, temp2
	add temp, temp2

	StockNumberLoopIncrease:
		cpi temp1, 0
		breq EndStockNumberLoopIncrease
		ld temp2, X+
		dec temp1
		rjmp StockNumberLoopIncrease

	EndStockNumberLoopIncrease:
		st X, temp

	sts StockNumber, temp
	call AdminMode

	
	rjmp END_INT

ReturnToStart:
	ldi temp, 1
	sts ScreenState, temp; Set ScreenState to admin mode
	call displaySelectItem
	
	rjmp END_INT


; Epilogue of push button 0
END_INT:
    pop temp1
	pop temp
    out SREG, temp
	pop temp
    reti            ; Return from the interrupt.
	
; subroutine for push button 1
; open the door

EXT_INT1:
    push temp
	in temp, SREG	; Prologue starts.
    push temp       ; Save all conflict registers in the prologue.
	push temp1       			; Prologue ends.
                    ; Load the value of the temporary counter.

	; debounce check
	cpi debounceFlag1, 1		; if the button is still debouncing, ignore the interrupt
	breq END_INT	
	ldi debounceFlag1, 1		; set the debounce flag
	ser r18;
	;out DDRC, r18; set Port C for output
	;ldi r18, 0xFF;
	;out PORTC, r18; write the pattern

	lds temp, ScreenState; check screen state
	cpi temp, 5
	breq DecreaseStock; decrease stock when right button is pressed in admin mode
	cpi temp, 2
	breq ReturnToStart

	rjmp END_INT

DecreaseStock:
	lds temp1, Input
	lds temp, StockNumber
	
	out DDRC, temp1
	out PORTC, temp1

	ldi XL, low(Inventory)
	ldi XH, high(Inventory)

	ldi temp2, 1
	sub temp1, temp2
	sub temp, temp2

	StockNumberLoopDecrease:
		cpi temp1, 0
		breq EndStockNumberLoopDecrease
		ld temp2, X+
		dec temp1
		rjmp StockNumberLoopDecrease

	EndStockNumberLoopDecrease:
		st X, temp

	sts StockNumber, temp
	call AdminMode

	
	rjmp END_INT
