keypad:
	ldi cmask, INITCOLMASK ; initial column mask
	clr col ; initial column
colloop:
	cpi col, 4
	breq keypad ; If all keys are scanned, repeat.
	STS PORTL, cmask ; Otherwise, scan a column.
	ldi temp1, 0xFF ; Slow down the scan operation.
delay: dec temp1
	brne delay
	LDS temp1, PINL ; Read PORTL
	andi temp1, ROWMASK ; get data from specific row
	cpi temp1, 0xF ; Check if any row is low, if all 1'a all high and hence no button press
	breq nextcol

ldi temp2, 10
Debounce: ;check button pressed for more than 10 ms (software debounce)
	rcall sleep_1ms 
	cpi temp2, 0 ;if counter has gotten to 0 it is not pressed
	breq unpressed ;clear pressed flag
	cpi temp2, 20 ;if counter has gotten to 1 it has been pressed
	breq end_debounce ;exit loop
	LDS temp1, PINL ; Read PORTL
	andi temp1, ROWMASK ; get data from specific row
	cpi temp1, 0xF ;check if button has been pressed
	breq decrement ;if not decrement the counter
	inc temp2 ;else increment 
	rjmp debounce;loop
	decrement:
	dec temp2 
	rjmp debounce

unpressed:
	clr pressed ;clear pressed flag and scan again
	rjmp keypad

end_debounce:
	cpi pressed,1  
	breq keypad ;if pressed flag is still set re-scan the keypad
	ldi pressed, 1 ;set pressed to 1

	; If yes, find which row is low
	ldi rmask, INITROWMASK ; Initialize for row check (top down)
	clr row ; 
	
	/*

keypad:
    ldi cmask, INITCOLMASK  ; initial column mask
    clr col                 ; initial column

colloop:
    cpi col, 4
    breq keypad               ; If all keys are scanned, repeat.
    sts PORTL, cmask        ; Otherwise, scan a column.
  
    ldi temp1, 0xFF         ; Slow down the scan operation.

delay:
    dec temp1
    brne delay              ; until temp1 is zero? - delay

    lds temp1, PINL          ; Read PORTL
    andi temp1, ROWMASK     ; Get the keypad output value
    cpi temp1, 0xF          ; Check if any row is low
    breq nextcol            ; if not - switch to next column

                            ; If yes, find which row is low
    ldi rmask, INITROWMASK  ; initialize for row check
    clr row

	*/

rowloop:
	cpi row, 4 ;to check all four rows
	breq nextcol ; the row scan is over.
	mov temp2, temp1 ;now temp 2 holds row data but gets changed each loop hence re-intialisation
	and temp2, rmask ; check un-masked bit
	breq convert ; if bit is clear, the key is pressed-clear mans and operation retuns all 0's 
	inc row ; else move to the next row
	lsl rmask ;look at the next row
	jmp rowloop
nextcol: ; if row scan is over
	lsl cmask ;change the col to next one
	inc cmask; after left shift ensure input data is still all 1's to enable pull up resistors
	inc col ; increase column value
	jmp colloop ; go to the next column
convert:
	cpi col, 3 ; If the pressed key is in col.3
	breq letters ; we have a letter
	; If the key is not in col.3 and
	cpi row, 3 ; If the key is in row3,
	breq symbols ; we have a symbol or 0
	mov temp1, row ; Otherwise we have a number in 1-9
	lsl temp1 ;row * 2
	add temp1, row ;row*2 + row
	add temp1, col ; temp1 = row*3 + col
	inc temp1

	;sts Digit, temp1
	jmp convert_end
letters:;ensure numbers to coressponding operations
	ldi temp1, 1
	sts SymbolFlag, temp1
	ldi temp1, 'A'
	add temp1, row
	cpi temp1, 'A'
	breq A
	cpi temp1, 'B'
	breq B
	cpi temp1, 'C'
	breq C
	cpi temp1, 'D'
	breq D
	rjmp convert_end
A:
	ldi temp1, 4
	sts SymbolFlag, temp1
	rjmp convert_end
B:
	ldi temp1, 5
	sts SymbolFlag, temp1
	rjmp convert_end
C:
	ldi temp1, 6
	sts SymbolFlag, temp1
	rjmp convert_end
D:
	rjmp convert_end
symbols:
	ldi temp1, 1
	sts SymbolFlag, temp1
	cpi col, 0 ; Check if we have a star
	breq star
	cpi col, 1 ; or if we have zero
	breq zero
	cpi col, 2 ; or if we have hash
	breq hash
	rjmp convert_end
star:
	ldi temp1, 2
	sts SymbolFlag, temp1
	rjmp convert_end
zero:
	rjmp convert_end
hash:
	ldi temp1, 3
	sts SymbolFlag, temp1
	rjmp convert_end

convert_end:                ;keypress is received
	
	// Screen state check to point ot differnt subroutines to handle input from keyboard

	lds temp, ScreenState			
	
	//Start state, skipping before 3 seconds
	cpi temp, 0
	breq startScreenKey			;screen is start, jump to startScreenKey 
	
	// Logic for when item is selected
	cpi temp, 1
	breq itemScreen

	// Logic for going back to select screen when hash is pressed on the insert coins screen
	cpi temp, 3
	breq InsertScreenJumper

	// Logic for the admin mode keypad inputs
	cpi temp, 5
	breq AdminScreen

	;cpi temp, 2
	;breq stockscreen

	rjmp loop

InsertScreenJumper:
	jmp InsertScreen

startScreenKey:
	ldi temp, 1
	sts ScreenState, temp    ;sets state to select screen
	rjmp loop				;jumps to mainloop to repeat cheaking states

itemScreen:
	lds temp, SymbolFlag
	cpi temp, 2
	breq GoToAdmin
	cpi temp, 0
	brne retloop				;if not equal, its a symbol, jump to retloop which restarts the keypad scan

	// Find number of stock
	ldi XL, low(Inventory)
	ldi XH, high(Inventory)
    
	sts Input, temp1

	ldi temp2, 1
	sub temp1, temp2

	//Loop through Inventory array to find seleced item
	SelectLoop:
		cpi temp1, 0
		breq EndSelectLoop
		ld temp2, X+
		dec temp1
		rjmp SelectLoop

	EndSelectLoop:
		ld temp1, X; Get the number of stock available
	
	cpi temp1, 0
	breq NoStock; If number of stock equals 0, go to empty screen
	ldi temp, 3
	sts ScreenState, temp         ;sets state to empty screen
	jmp loop ;jumps to mainloop to repeat cheaking states

	NoStock:
		ldi temp, 2
		sts ScreenState, temp         ;sets state to empty screen
		jmp loop ;jumps to mainloop to repeat cheaking state

// Loop through keyboard again
retloop:
	clear SymbolFlag
	rjmp keypad 

GoToAdmin:
	clear SymbolFlag
	ldi temp, 5
	sts ScreenState, temp; Set ScreenState to admin mode
	
	ldi temp1, 1 //Default Item
	sts AdminFlag, temp1
	rjmp loop

IncreaseCostJumper:
	jmp IncreaseCost

DecreaseCostJumper:
	jmp DecreaseCost

ClearInventoryJumper:
	jmp ClearInventory

AdminScreen:
	// Checks which symbol / letter was pressed and directs to the correct logic
	lds temp, SymbolFlag
	cpi temp, 3
	breq BackToSelect; Hash -> back to select screen
	cpi temp, 4
	breq IncreaseCostJumper; A -> increase cost
	cpi temp, 5
	breq DecreaseCostJumper; B -> decrease cost
	cpi temp, 6
	breq ClearInventoryJumper; C -> clear stock
	cpi temp, 0
	brne retloop

	// Get Inventory Number
	ldi XL, low(Inventory)
	ldi XH, high(Inventory)
    
	lds temp, AdminFlag
	cpi temp, 1
	brne FindStockNumber

	// Initial case defaulting to 1
	
	ldi temp1, 1
	
	rjmp FindStockNumber

	FindStockNumber:

	sts Input, temp1
	ldi temp2, 1
	sub temp1, temp2
	
	//Loop through Inventory array to find stock
	StockNumberLoop:
		cpi temp1, 0
		breq EndStockNumberLoop
		ld temp2, X+
		dec temp1
		rjmp StockNumberLoop

	EndStockNumberLoop:
		ld temp1, X; Get number of stock

	sts StockNumber, temp1; save number of stock

	lds temp1, Input; get item  number again

	// Get Cost For Item
	ldi XL, low(Costs)
	ldi XH, high(Costs)
    
	sts Input, temp1

	ldi temp2, 1
	sub temp1, temp2

	// Loop through cost array to find item cost
	CostLoop:
		cpi temp1, 0
		breq EndCostLoop
		ld temp2, X+
		dec temp1
		rjmp CostLoop

	EndCostLoop:
		ld temp1, X; get item cost

	sts ItemCost, temp1

	clr temp
	sts AdminFlag, temp; clear admin flag to not default to 1

	jmp loop

// Return to select when hash is pressed on input screen
InsertScreen:
	lds temp, SymbolFlag
	cpi temp, 3; check if hash was pressed
	breq BackToSelect
	rjmp retloop

// REturn back to select screen
BackToSelect:
	clear SymbolFlag
	ldi temp, 1
	sts ScreenState, temp; Set ScreenState to select mode
	rjmp loop

// loop for 5 seconds while star is held down
/*AdminModeLoop:
	push temp
	lds temp, AdminLoopCount

	out DDRC, temp
	out PORTC, temp ; Write value to PORTC

	cpi temp, 5
	breq AdminAccessGranted

	inc temp
	sts AdminLoopCount, temp

	pop temp
	rjmp retloop

AdminAccessGranted:
	clear SymbolFlag
	ldi temp, 5
	sts ScreenState, temp; Set ScreenState to admin mode
	pop temp
	jmp loop*/

// Increase cost when B is pressed on the admin screen	
IncreaseCost:
	lds temp1, Input
	lds temp, ItemCost

	ldi XL, low(Costs)
	ldi XH, high(Costs)

	ldi temp2, 1
	sub temp1, temp2
	add temp, temp2

	// Loop through cost array
	CostLoopIncrease:
		cpi temp1, 0
		breq EndCostLoopIncrease
		ld temp2, X+
		dec temp1
		rjmp CostLoopIncrease

	EndCostLoopIncrease:
		st X, temp; store new cost

	sts ItemCost, temp; save new cost for display to use

	call AdminMode

	jmp retloop; return to keyboard loop

//Decrease cost when B is pressed on admin screen
DecreaseCost:
	lds temp1, Input
	lds temp, ItemCost; get current cost
	
	out DDRC, temp
	out PORTC, temp

	ldi XL, low(Costs)
	ldi XH, high(Costs)

	ldi temp2, 1
	sub temp1, temp2
	sub temp, temp2

	//Loop hrough cost array
	CostLoopDecrease:
		cpi temp1, 0
		breq EndCostLoopDecrease
		ld temp2, X+
		dec temp1
		rjmp CostLoopDecrease

	EndCostLoopDecrease:
		st X, temp; store new value in cost array

	sts ItemCost, temp; store new cost for display

	call AdminMode; update display

	jmp retloop; return to keyboard loop

// Clears stock when C is pressed
ClearInventory:
	lds temp1, Input
	lds temp, 1
	
	ldi XL, low(Inventory)
	ldi XH, high(Inventory)

	ldi temp2, 1
	sub temp1, temp2

	//Loop thorgh inventory array
	StockNumberLoopClear:
		cpi temp1, 0
		breq EndStockNumberLoopClear
		ld temp2, X+
		dec temp1
		rjmp StockNumberLoopClear

	EndStockNumberLoopClear:
		st X, temp; store new inventory

	sts StockNumber, temp

	call AdminMode; update display

	jmp retloop