Timer0OVF: ; interrupt subroutine to Timer0
    in temp, SREG
    push temp       ; Prologue starts.
	push temp1
    push YH         ; Save all conflict registers in the prologue.
    push YL
	push r27
	push r26
	; Prologue ends.

		lds temp, screenstate			
		cpi temp, 0
		breq startscreenthreesec
		cpi temp, 2
		breq startscreenthreesec
		rjmp endstartscreenthreesec

	/*;Half second counter
	HalfSec:			
		clr r26
		clr r27
		clr temp
	
		lds r26, HalfSecondCounter
    	lds r27, HalfSecondCounter+1
    	adiw r27:r26, 1 		; Increase the turntable counter by one.
    	cpi r26, low(19530)     ; 2.5 seconds 19530
    	ldi temp, high(19530)
    	cpc temp, r27
    	brne notHalfSec			; .5s have not passed
		rcall startScreenChange

	endstartScreenThreeSec:
		rjmp checkPBDebounce

	endPBDebounce:
		rjmp EndIF	

	notHalfSec: 				; Store the new value of the turntable counter.
		sts HalfSecondCounter, r26
		sts HalfSecondCounter+1, r27
		rjmp EndIF*/

	;start screen 3 seconds
	startScreenThreeSec:			
		clr r26
		clr r27
		clr temp
	
		lds r26, ThreeSecondCounter
    	lds r27, ThreeSecondCounter+1
    	adiw r27:r26, 1 		; Increase the turntable counter by one.
    	cpi r26, low(23436)     ; 2.5 seconds 19530
    	ldi temp, high(23436)
    	cpc temp, r27
    	brne notThreeSec			; 2.5s have not passed
		;ldi temp, 1
		;sts ScreenState, temp
		rcall displaySelectItem
		;lds temp, ScreenState			
		;cpi temp, 2
		;brne endstartScreenThreeSec
	
	/*ledEmptyScreen:
		ldi temp1, 0xFF              ; PORTC is output
		out DDRC, temp1
		out PORTC, temp1
		out DDRG, temp1
		out PORTG, temp1*/

	endstartScreenThreeSec:
		rjmp checkPBDebounce
	
	endPBDebounce:
		rjmp EndIF	

	notThreeSec: 				; Store the new value of the turntable counter.
		sts ThreeSecondCounter, r26
		sts ThreeSecondCounter+1, r27
		rjmp endstartScreenThreeSec
   
EndIF:
    pop r26         ; Epilogue starts;
    pop r27         ; Restore all conflict registers from the stack.
    pop YL
    pop YH
	pop temp1
    pop temp
    out SREG, temp
    reti            ; Return from the interrupt.